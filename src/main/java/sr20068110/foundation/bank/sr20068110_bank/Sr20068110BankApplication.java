package sr20068110.foundation.bank.sr20068110_bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sr20068110BankApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sr20068110BankApplication.class, args);
	}

}
