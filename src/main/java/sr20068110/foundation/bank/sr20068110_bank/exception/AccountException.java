package sr20068110.foundation.bank.sr20068110_bank.exception;

public class AccountException extends Exception {
	
	public AccountException() {
		super();
	}
	
	public AccountException(String mesage) {
		super(mesage);
	}
}
