package sr20068110.foundation.bank.sr20068110_bank.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(AccountException.class)
	@ResponseStatus (value=HttpStatus.PROCESSING)
	public @ResponseBody ExceptionResponse handleAccountExceptions (AccountException e, HttpServletRequest request) {
		
		ExceptionResponse response = new ExceptionResponse();
		response.setErrorMessage(e.getMessage());
		response.setRequestURI(request.getRequestURI());
		return response;
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus (value=HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse handleAllExceptions (Exception e, HttpServletRequest request) {
		
		ExceptionResponse response = new ExceptionResponse();
		response.setErrorMessage(e.getMessage());
		response.setRequestURI(request.getRequestURI());
		return response;
	}
}
