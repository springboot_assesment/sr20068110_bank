package sr20068110.foundation.bank.sr20068110_bank.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;

/**
 * @author Srinivas
 *
 */
@Entity
public class SRCustomer {


	@Id
	@GeneratedValue
	private int customerId;
	
	private String name;
	
	private String fullAddress;
	
	private String contactNumber;
	
	private String aadharNumber;
	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public List<SRAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<SRAccount> accounts) {
		this.accounts = accounts;
	}
	
	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}



	@OneToMany(targetEntity=SRAccount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="customerId",referencedColumnName="customerId")
	List<SRAccount> accounts ;
	
	@OneToMany(targetEntity=Address.class,cascade=CascadeType.ALL)
	@JoinColumn(name="customerId",referencedColumnName="customerId")
	List<Address> address ;
	
}
