package sr20068110.foundation.bank.sr20068110_bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sr20068110.foundation.bank.sr20068110_bank.entity.Address;

public interface SRAddressRepository extends JpaRepository<Address, Integer> {

}
