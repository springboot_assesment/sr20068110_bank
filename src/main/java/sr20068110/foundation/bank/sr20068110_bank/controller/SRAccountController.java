package sr20068110.foundation.bank.sr20068110_bank.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.exception.AccountException;
import sr20068110.foundation.bank.sr20068110_bank.service.SRAccountServiceImpl;

@RestController
public class SRAccountController {
	
	@Autowired
	SRAccountServiceImpl accService;
	
	@PostMapping("/account")
	public SRAccount addCustomer(@RequestBody SRAccount account) {
		System.out.println("addCustomer() ");
		return accService.AddAccount(account);
	}
	
	@PutMapping("/account")
	public SRAccount updateAccount(@RequestBody SRAccount account) {
		System.out.println("updateAccount() ");
		return accService.updateAccount(account);
	}
	
	@GetMapping("/account/all")
	public List<SRAccount> getAllAccounts() throws Exception {
		System.out.println("getAllAccounts() ");
		List<SRAccount> accounts = accService.getAllSRAccounts();
		if (0 == accounts.size())
			throw new Exception("No Accounts found");
		else 
		return accounts;
	}
	
	
	@GetMapping("/account")
	public List<SRAccount> getAllAccountCustomers() throws Exception {
		System.out.println("getAllCustomers() ");
		List<SRAccount> customers = accService.getAllSRAccounts();
		if (0 == customers.size())
			throw new Exception("No Customers found");
		else 
		return accService.getAllSRAccounts();
	}
	
	/*@DeleteMapping("/account/{accountNumber}")
	public String deleteAccountById(@PathVariable int accountNumber) {
		return accService.deleteAccByNumber(accountNumber);
		
	}
	
	
	@DeleteMapping("/account")
	public String deleteAllAccounts() {
		return accService.deleteAllAcccounts();
		
	}*/
	
	@PostMapping("/account/transfer")
	public String transferBalance(@RequestParam(value="fromAccount") int fromAccount,
			@RequestParam(value="toAccount") int toAccount, @RequestParam(value="amount") BigDecimal amount) throws AccountException {
		System.out.println("transferBalance() ");
		return accService.transferFunds(fromAccount, toAccount, amount);
	}
	
}
