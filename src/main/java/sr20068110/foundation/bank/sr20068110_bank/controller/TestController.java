package sr20068110.foundation.bank.sr20068110_bank.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/")
	public String welcome(){
		return "get working!!";
	}
	
}
