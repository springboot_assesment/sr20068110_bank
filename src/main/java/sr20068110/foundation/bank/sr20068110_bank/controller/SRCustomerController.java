package sr20068110.foundation.bank.sr20068110_bank.controller;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import sr20068110.foundation.bank.sr20068110_bank.exception.AccountException;
import sr20068110.foundation.bank.sr20068110_bank.dto.CustomerRequest;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRCustomer;
import sr20068110.foundation.bank.sr20068110_bank.service.SRAccountServiceImpl;
import sr20068110.foundation.bank.sr20068110_bank.service.SRCustomerServiceImpl;

@RestController
public class SRCustomerController {
	
	@Autowired
	SRAccountServiceImpl accService;
	
	@Autowired
	SRCustomerServiceImpl cusService;
	
	
	@GetMapping("/customer")
	public List<SRCustomer> getAllCustomers() throws Exception {
		System.out.println("getAllCustomers() ");
		List<SRCustomer> customers =cusService.getAllSRCustomers();
		if (0 == customers.size())
			throw new Exception("No Customers found");
		else 
		return cusService.getAllSRCustomers();
	}
	
	
	@PostMapping("/customer")
	public ResponseEntity<Object> saveCustomer(@RequestBody CustomerRequest customer) {
		System.out.println("getAllCustomers() ");
		 cusService.createSRCustomer(customer);
		return (ResponseEntity<Object>) ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(customer).toUri()).build();
	}
	
	@PutMapping("/customer")
	public ResponseEntity<Object> updateCustomer(@RequestBody SRCustomer customer) {
		System.out.println("getAllCustomers() ");
		 cusService.updateSRCustomer(customer);
		return (ResponseEntity<Object>) ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(customer.getCustomerId()).toUri()).build();
	}
	
	@DeleteMapping("/customer")
	public String deleteCustomer(@RequestBody SRCustomer customer) {
		System.out.println("getAllCustomers() ");
		return cusService.deleteSRCustomer(customer.getCustomerId());
	}
	
	
	@GetMapping("/customer/{accountNumber}")
	public SRAccount getAccByNumber(@PathVariable int accountNumber) throws AccountException{
		System.out.println("getAccByNumber() ");
		SRAccount account = accService.getAccountByNumber(accountNumber);
		if (null == account)
			throw new AccountException("Account not exists");
		else
			return account;
	}
	
	@GetMapping("/account/{accountNumber}/balance")
	public BigDecimal getAccountBalance(@PathVariable int accountNumber) {
		System.out.println("getAccountBalance() ");
		return accService.getBalanceOf(accountNumber);
	}
	
}
