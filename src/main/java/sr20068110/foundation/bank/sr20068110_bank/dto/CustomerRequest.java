package sr20068110.foundation.bank.sr20068110_bank.dto;

import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRCustomer;

public class CustomerRequest {
	
	
	private SRCustomer customer;

	public SRCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(SRCustomer customer) {
		this.customer = customer;
	}
	

	
}
