package sr20068110.foundation.bank.sr20068110_bank.dto;

import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRCustomer;

public class AccountRequest {
	
	private SRAccount account;

	public SRAccount getAccount() {
		return account;
	}

	public void setAccount(SRAccount account) {
		this.account = account;
	}
	
	

}
