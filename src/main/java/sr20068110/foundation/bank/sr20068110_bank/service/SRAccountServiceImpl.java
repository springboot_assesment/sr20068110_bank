package sr20068110.foundation.bank.sr20068110_bank.service;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sr20068110.foundation.bank.sr20068110_bank.exception.AccountException;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRAccountRepository;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRCustomerRepository;

@Service
public class SRAccountServiceImpl {
	
	
	@Autowired
	SRCustomerRepository customerrepo;
	
	@Autowired
	SRAccountRepository accountrepo;
	
	public SRAccount AddAccount(SRAccount acc) {
		return accountrepo.save(acc);
	}
	
	public SRAccount getAccountByNumber(int accountNumber){
		
		Optional<SRAccount>  acc = accountrepo.findById(accountNumber);
		
		return acc.get();
		
	}
	
	public SRAccount updateAccount(SRAccount acc){
		
		return accountrepo.save(acc);
	}
	
	public List<SRAccount> getAllSRAccounts() {
		
		return accountrepo.findAll();
	}
	
	/*public void deleteAccByNumber(int accountNumber){
		
		return accountrepo.deleteById(accountNumber);
	}
	
	public void deleteAllAcccounts(){
		
		return accountrepo.deleteAll();
	}
	*/

	
	public String transferFunds(int fromAccount, int toAccount, BigDecimal amount) throws AccountException {
		String message = null;
		Optional<SRAccount> fromAcc =  accountrepo.findById(fromAccount);
		SRAccount fAcc = fromAcc.get();
		if (null != fAcc.getAmount() && fAcc.getAmount().compareTo(amount) > 0) {
			Optional<SRAccount> toAcc = accountrepo.findById(toAccount);
			SRAccount tAcc = toAcc.get();
			BigDecimal updatedToAmount = tAcc.getAmount().add(amount);
			tAcc.setAmount(updatedToAmount);
			accountrepo.save(tAcc);
			BigDecimal updatedFromAmount = fAcc.getAmount().subtract(amount);
			fAcc.setAmount(updatedFromAmount);
			accountrepo.save(fAcc);
			message = "Amount transfer succesful";
		}else {
			message = "Account has Insufficient balance!" ;
			throw new AccountException(message);
		}
		return message;
	}
	
	public BigDecimal getBalanceOf(int accountNumber) {
		Optional<SRAccount> fromAcc =  accountrepo.findById(accountNumber);
		
		return fromAcc.get().getAmount();
	}
	

}
