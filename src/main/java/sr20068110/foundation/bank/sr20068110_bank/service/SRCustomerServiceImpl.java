package sr20068110.foundation.bank.sr20068110_bank.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sr20068110.foundation.bank.sr20068110_bank.dto.CustomerRequest;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRCustomer;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRAccountRepository;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRCustomerRepository;

@Service
public class SRCustomerServiceImpl {
	
	@Autowired
	SRCustomerRepository customerrepo;
	
	@Autowired
	SRAccountRepository accountrepo;
	
	public SRCustomer createSRCustomer(CustomerRequest srCustomer)
	{
		//Business Logic
		SRCustomer savedSRCustomer=customerrepo.save(srCustomer.getCustomer());
	return savedSRCustomer;
	}
	
	public Optional<SRCustomer> findSRCustomer(Integer id)
	{
	//Business Logic
	return customerrepo.findById(id);
	}
	
	public SRCustomer updateSRCustomer(SRCustomer srCustomer)
	{
	//Business logic
	return  customerrepo.save(srCustomer);
	}
	
	public String deleteSRCustomer(Integer id)
	{
		customerrepo.deleteById(id);
		return "Success";
	}
	
	public List<SRCustomer> getAllSRCustomers()
	{
	//business logic
	return customerrepo.findAll();
	}
	
}
