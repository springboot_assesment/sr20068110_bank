package sr20068110.foundation.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import sr20068110.foundation.bank.sr20068110_bank.Sr20068110BankApplication;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;

@SpringBootTest(classes=Sr20068110BankApplication.class,webEnvironment=WebEnvironment.RANDOM_PORT)
public class SRAccountControllerIntegrationTest {

	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate ;
//	private SRAccountController restcontroller ;
	
	@Test
	public void testgetAllAccounts() {
		
		SRAccount[] acc = this.restTemplate.getForObject("http://localhost:" + port+"/account", SRAccount[].class);
		 System.out.println("acc..  "+acc);
		 assertEquals(1, acc.length);
		 
	}
}
