package sr20068110.foundation.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.*;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sr20068110.foundation.bank.sr20068110_bank.dto.CustomerRequest;
import sr20068110.foundation.bank.sr20068110_bank.entity.Address;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRCustomer;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRCustomerRepository;
import sr20068110.foundation.bank.sr20068110_bank.service.SRCustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class SRCustomerServiceTest {
	
	@InjectMocks
	SRCustomerServiceImpl custService;
	
	@Mock
	SRCustomerRepository custRepo;
	
	@Test
	public void createCustomers() {
		
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		CustomerRequest cr = new CustomerRequest();
		cr.setCustomer(customer);
		custService.createSRCustomer(cr);
		verify(custRepo, times(1)).save(customer);
	}
	
	@Test
	public void getcustomers() {
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		List<SRCustomer> custList =new ArrayList<SRCustomer>();
		custList.add(customer);
		when(custRepo.findAll()).thenReturn(custList) ;
		assertEquals(1, custService.getAllSRCustomers().size());
	}
	
	@Test
	public void deleteCustomer() {
		custService.deleteSRCustomer(1);
		verify(custRepo, times(1)).deleteById(1);
	}

	
	@Test
	public void findCustomer() {
		custService.findSRCustomer(1);
		verify(custRepo, times(1)).findById(1);
	}
	
	@Ignore
	@Test
	public void updateCustomer() {
		
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		custService.updateSRCustomer(customer);
		verify(custRepo, times(1)).saveAndFlush(customer);
	}
	
	
}
