package sr20068110.foundation.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.exception.AccountException;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRAccountRepository;
import sr20068110.foundation.bank.sr20068110_bank.service.SRAccountServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class SRAccountServiceTest {

	@InjectMocks
	SRAccountServiceImpl accountService;
	
	@Mock
	SRAccountRepository accountRepo;
	
	
	
	@Ignore
	@Test
	public void findAccount() {
		accountService.getAccountByNumber(2);
		verify(accountRepo, times(2)).findById(2);
	}

	@Ignore
	@Test
	public void transferFunds() throws AccountException   {
			SRAccount fAcc = new SRAccount();
			SRAccount tAcc = new SRAccount();
			tAcc.setAmount(BigDecimal.TEN);
			fAcc.setAmount(BigDecimal.ZERO);
			when(accountRepo.save(fAcc)).thenReturn(fAcc) ;
			when(accountRepo.save(tAcc)).thenReturn(tAcc) ;

			accountService.transferFunds(1,2,BigDecimal.TEN);
			
			Assert.assertEquals(fAcc.getAmount(), BigDecimal.ZERO);
			Assert.assertEquals(tAcc.getAmount(), BigDecimal.TEN);

	}
	
	@Test
	public void updateAccount() {
		// Sample test case failed scenario
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		accountService.updateAccount(account);
		verify(accountRepo, times(1)).saveAndFlush(account);
	}
	
	@Test
	public void getAccounts() {
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		when(accountRepo.findAll()).thenReturn(accountList) ;
		assertEquals(1, accountService.getAllSRAccounts().size());
	}
	
	@Test
	public void addAccount() {
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
	
		accountService.AddAccount(account);
		verify(accountRepo, times(1)).save(account);
	}
}
