package sr20068110.foundation.bank.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import sr20068110.foundation.bank.sr20068110_bank.entity.Address;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRAccount;
import sr20068110.foundation.bank.sr20068110_bank.entity.SRCustomer;
import sr20068110.foundation.bank.sr20068110_bank.repository.SRCustomerRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SRCustomerRepositoryTest{

	@Autowired
	SRCustomerRepository custRepo;
	
	@Test
	public void createCustomersTest() {
		
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		custRepo.save(customer);
		assertEquals(1,customer.getAccounts().size());
	}
	
	@Test
	public void getcustomersTest() {
		List<SRCustomer> cust = custRepo.findAll();
		assertEquals(0,cust.size());
		
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		custRepo.save(customer);
		
		List<SRCustomer> cust1 = custRepo.findAll();
		assertEquals(1,cust1.size());
	}
	
	@Ignore
	@Test
	public void deleteCustomerTest() {
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		custRepo.save(customer);
		custRepo.delete(customer);
		Assert.assertNull(customer);
	//	verify(custRepo, times(1)).deleteById(1);
		
	}

	
	@Test
	public void findCustomerTest() {
		Optional<SRCustomer> cust = custRepo.findById(1);
		assertEquals(true,cust.isPresent());
	}
	
	@Test
	public void updateCustomerTest() {
		
		SRCustomer customer = new SRCustomer();
		customer.setName("Sri");
		customer.setAadharNumber("123456789");
		customer.setContactNumber("2212345123");
		SRAccount account = new SRAccount();
		account.setAccountType("Savings");
		account.setAmount(new BigDecimal(1000.00));
		List<SRAccount> accountList = new ArrayList<SRAccount>();
		accountList.add(account);
		customer.setAccounts(accountList);
		Address address = new Address();
		List<Address> addr = new ArrayList<Address>();
		addr.add(address);
		customer.setAddress(addr);
		SRCustomer repo = custRepo.saveAndFlush(customer);
	//	verify(custRepo, times(1)).saveAndFlush(customer);
		assertNotNull(repo);
	}
	
}
